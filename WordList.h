#ifndef WORDLIST_H
#define WORDLIST_H
#include <string>
#include <iostream>
#include "WordData.h"

// Extract all words of input file
// Keep track of which line the word appeared
// WordList object stores word & line number in WordNode

class WordList {
private:
	std::string _fileName;

	struct wordNode {
		WordData wordData;
		wordNode* nextNode = NULL;

		wordNode(WordData data) {
			//nextNode = node;
			wordData = data;
            
		}
		wordNode(){

		}

	};

	wordNode* _headNode = NULL;
	wordNode* _endNode = NULL;
	wordNode* _tempNode = NULL;
    int listSize {0};

public:
	// WordList public methods:
	WordList(std::string);
    ~WordList();
	void parseFile(std::ostream& stream);
	void addNodeToEnd(WordData tempWordData,int currentLine,wordNode* _endNode);
    bool checkExistingWord(std::string word,wordNode* traverse);
    void addLineToWord(std::string word,int currentLine,wordNode* traverse);
	void travelWordList(wordNode* traverse);
	void displayWordList(wordNode* traverse, std::ostream& stream);
    void sortWordList(wordNode*& listToSort);
    int getListSize(wordNode* start);
    std::string getWordLower(std::string str);
    
};

#endif