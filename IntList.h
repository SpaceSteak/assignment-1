/*
Assignment 1 - COMP 5421
Marc Belley - 5701295
*/

#ifndef INTLIST_H
#define INTLIST_H

class IntList{
private:
	int listSize;
	int listCapacity;
	int* listValuesPtr;


public:
	IntList();
    IntList(const IntList& intList);
    IntList& operator=(const IntList& intList) ;
	void printList(IntList list);
    void lineNumList();
	//IntList(IntList intList);
	//bool IntFull();
	int getSize();
	int getCapacity();
	void setSize(int value);
	void setCapacity(int value);
	void appendInt(int value);
    bool checkIntInList(int lineNum);
    
	void setElt(int index,int value);
	int getElt(int index);

	int* getPointer(); // needs to be read-only!!

	int* doubleList(int* ptrToDouble);
	bool isFull() const;
	bool isEmpty() const;
};

#endif
