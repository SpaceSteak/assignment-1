/*
Assignment 1 - COMP 5421
Marc Belley - 5701295
*/

#include "WordList.h"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <cctype>
#include <locale>
#include <iomanip>

using namespace std;

WordList::WordList(std::string inputName){

	// store the file name in the WordList
	_fileName = inputName;

}

WordList::~WordList(){
    // clean up wordlist by deleting all the nodes it points it to
    wordNode* traverse = new wordNode{};
    wordNode* temp = new wordNode{};
    traverse = _headNode;
    while (traverse->nextNode != NULL) {
		//cout << traverse->wordData.getWord() << " being deleted" << endl;
        temp = traverse;
        traverse = traverse->nextNode;
        delete temp;
	}
}


void WordList::addNodeToEnd(WordData tempData, int currentLine, wordNode* _endNode){
	
    // new node
    wordNode* newNode = new wordNode;
    newNode->wordData = tempData;
    newNode->nextNode = NULL;
    
    // temp pointer
    wordNode* tmpPtr = _headNode;
    if (tmpPtr != NULL)
    {
        while ( tmpPtr->nextNode != NULL) {
            tmpPtr = tmpPtr->nextNode;
        }
        // now at last node
        tmpPtr->nextNode = newNode;
    }
    else {
        // first node in list
        _headNode = newNode;
    }
    
}

void WordList::parseFile(ostream& stream){
	int currentLine{1};
    string inputLine;
	WordData tempWordData;
    string currentWord;
    vector<string> words;

	std::cout << "Reading from file: " << _fileName << std::endl;
    ifstream inputFile(_fileName);

	if (inputFile.is_open())
	{
		while (getline(inputFile, inputLine))
		{
			//std::cout << inputLine << '\n';
			istringstream parsedLine(inputLine);
            
			//cout << "transforming tempWordData to line number: " << currentLine << endl;
            
            // read next word in line
			while (parsedLine >> currentWord) {
                if (currentWord == "C++") currentWord = "C"; // c++ fix
				for (size_t i = 0; i < currentWord.length(); ++i)
                    // clean up the word for alphanum chars
                    if (!isalnum(currentWord[i]))
                        currentWord.erase(i, 1);
				tempWordData = WordData(currentWord,currentLine);

				// iterate through wordlist to check if word already exists before adding, if so only add line to node

				if (_headNode != NULL){
					if (checkExistingWord(currentWord,_headNode) == 1) {
                        addLineToWord(currentWord,currentLine,_headNode);
                    }
					else {
						addNodeToEnd(tempWordData, currentLine, _endNode);
						//cout << endl << "added '" << currentWord << "' to WordList!" << endl;
					}
				}
				else {
					addNodeToEnd(tempWordData, currentLine, _endNode);
                    listSize++;
					//cout << endl << "added '" << currentWord << "' to WordList!" << endl;
				}
			}
            currentLine++;
		}
		inputFile.close();
		cout << "\nFinished parsing file... getting ready to display output!!\n";
        sortWordList(_headNode);
		//travelWordList(_headNode);
        displayWordList(_headNode, stream);
        
	}
	else cout << "Unable to open file";

}


bool WordList::checkExistingWord(string word,wordNode* traverse) {

    while (traverse->nextNode != NULL) {
        if (getWordLower(traverse -> wordData.getWord()) == getWordLower(word)) {
            //cout << " word " << traverse-> wordData.getWord() << " found. Appending the line num";
            return 1;
        }
        else {
          //cout << traverse->getWord() << endl;
            traverse = traverse->nextNode;
        }
    }
    return 0;
}

void WordList::addLineToWord(string word,int currentLine,wordNode* traverse){
    while (traverse->nextNode != NULL) {
        if (getWordLower(traverse -> wordData.getWord()) == getWordLower(word)) {
            //cout << "analyzing word " << traverse -> wordData.getWord() << endl;
            traverse->wordData.appendLineNum(currentLine);
            return;
        }
        else {
            //cout << traverse->getWord() << endl;
            traverse = traverse->nextNode;
            
        }
    }
    
}

void WordList::travelWordList(wordNode* traverse) {
	//cout << traverse->wordData.getWord() << " happens " << traverse->wordData.getFrequency() << endl;
	while (traverse->nextNode != NULL) {
		cout << traverse->wordData.getWord() << " happens " << traverse->wordData.getFrequency() << endl;
			traverse = traverse->nextNode;
	}

}

void WordList::displayWordList(wordNode* traverse,ostream& stream) {
    int i{97};
    const int WIDTH{15};
    
    while (i < 123) {
        cout << "<" << char(std::toupper(char(i))) << ">" << endl;
        while ((traverse->nextNode != NULL) && std::tolower(traverse->wordData.getWord()[0])==i) {
            cout << setw(WIDTH) << traverse->wordData.getWord() << " (" << traverse->wordData.getFrequency() <<")" << traverse->wordData.lineNumString()<< endl;
            traverse = traverse->nextNode;
        }
        i++;
    }
}

void WordList::sortWordList(wordNode*& listToSort) {
    string str1;
    string str2;
    
    // traverse the list but if the next node's WordData.getWord() is lower, then
    // switch over the pointers
    
    int size = getListSize(listToSort);
    //int i = 0;
    
    while(size--)
    {
        wordNode* current = listToSort;
        wordNode* prev = NULL; // We are at the beginnig, so there is no previous node.
        
        while(current->nextNode != NULL) // We have at least one node (size > 0) so `current` itself is not NULL.
        {
            wordNode *after = current->nextNode;
            
            str1 = current->wordData.getWord();
            std::transform (str1.begin(), str1.end(), str1.begin(), ::tolower);
            
            str2 = after->wordData.getWord();
            std::transform(str2.begin(), str2.end(), str2.begin(), ::tolower);
            
            if (str1.compare(str2) > 0)
            {
                //swap the items
                //cout << "swapped items" << endl;
                current->nextNode = after->nextNode;
                after->nextNode = current;
                if (prev == NULL)
                    listToSort = after;
                else
                    prev->nextNode = after;
                
                prev = after;
            }
            else
            {
                prev = current;
                current = current->nextNode;
            }
        }
    }
    
}

int WordList::getListSize(wordNode* start)
{
    int count = 0;
    while(start != NULL)
    {
        count++;
        start = start->nextNode;
    }
    return count;
}


string WordList::getWordLower(string str) {
    std::locale loc;
    for (std::string::size_type i=0; i<str.length(); ++i)
        str[i] = std::tolower(str[i],loc);
    return str;
    
}

