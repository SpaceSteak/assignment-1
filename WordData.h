#ifndef WORDDATA_H
#define WORDDATA_H
#include "IntList.h"
#include <string>

class WordData {
private:
	char* wordChars;
	int wordFrequency;
	IntList lineNums;
public:
	WordData();
    WordData(std::string word,int lineNum);
    std::string lineNumString();
	void appendLineNum(int lineNum);
	int getFrequency();
	char* getWord();
};



#endif