/*
Assignment 1 - COMP 5421
Marc Belley - 5701295
*/

#define _SCL_SECURE_NO_WARNINGS
#include "IntList.h"
#include <iostream>

/*
	Array-based list stores 3 data members
	1. pointer to dynamically allocated array of int elements
	2. size of list
	3. current capacity
*/

// constructor
IntList::IntList(){
	listSize = 0;
	listCapacity = 1;

	// ** check if list values should be a pointer to a new int array??
	listValuesPtr = new int[listSize];
}

// copy constructor
IntList::IntList(const IntList& intList) {
    //need to make a copy of the list listValuesPtr points to and a copy of the pointer
    listValuesPtr = nullptr;
    listSize = intList.listSize;
    listCapacity = intList.listCapacity;
    if (intList.isEmpty()) {
        return;
    }
    else {
        listValuesPtr = new int[intList.listSize];
        for (int i = 0;i <= intList.listSize;i++) {
            listValuesPtr[i] = intList.listValuesPtr[i];
        }
        
        
    }
    
}

IntList& IntList::operator=(const IntList& rhs) {
    // check self assignment
    if (this == &rhs) return *this;
    else{
        IntList temp(rhs);
        this->listSize = temp.listSize;
        this->listCapacity = temp.listCapacity;
        this->listValuesPtr = temp.listValuesPtr;
        return *this;
        
    }
    
}

int IntList::getSize(){
	return listSize;
}

int IntList::getCapacity(){
	return listCapacity;

}
void IntList::setSize(int value){

}
void IntList::setCapacity(int value){

}
void IntList::appendInt(int value){
    if (checkIntInList(value)) return;
	listValuesPtr[listSize] = value;
	listSize++;
	if (isFull()) {
		//std::cout << std::endl << "Tried appending to full list. Initiating doubling procedure.";
		listValuesPtr = doubleList(listValuesPtr);
	}
}

bool IntList::checkIntInList(int lineNum) {
    //std::cout << "int list size: " << this->getSize() << std::endl;
    for(int i = 0; i < this->getSize();i++) {
        //std::cout << "checking " << lineNum << " vs  "<< this->getElt(i) << " to so if already in line" << std::endl;
        if (lineNum == this->getElt(i)){
            //std::cout << "found matching line num entry for word" << std::endl;
            return 1;
        }
    }
    return 0;
}

bool IntList::isFull() const{
	return (listSize == listCapacity);
}

bool IntList::isEmpty() const {
	return (listSize == 0);
}

int* IntList::doubleList(int* ptrToDouble){

	//make an array to store old array
	int* temp = new int[listSize];

	//copy old list into temp array
	std::copy(ptrToDouble, ptrToDouble + listSize, temp);

	// delete the old, full array
	//delete[] ptrToDouble;

	// double the size, double the pleasure
	listCapacity = listSize * 2;

	// remake the bigger array
	ptrToDouble = new int[listCapacity];

	// copy temp data to it
	std::copy(temp, temp + listSize, ptrToDouble);

	delete[] temp;
	return ptrToDouble;
}

int IntList::getElt(int index){
	if (index <= listSize)
		return listValuesPtr[index];
	else
		std::cout << "\n\n*** Error in IntList Class: Attempting to retrieve value past current size.";
    return 0;
}
void IntList::setElt(int index,int value){
	listValuesPtr[index] = value;
}
int* IntList::getPointer(){ // needs to be read-only!! make a new const pointer to use
	return listValuesPtr;
}

void IntList::printList(IntList list){
	std::cout << "current list size: " << getSize() << std::endl;
	std::cout << "current list capacity: " << getCapacity() << std::endl;
	std::cout << "current list content: " << std::endl;
	for (int i = 0; i < getSize(); i++){
		std::cout << "value at index --- " << i << " --- " << getElt(i) << std::endl;
	}
}

void IntList::lineNumList(){
	for (int i = 0; i < getSize(); i++){
		std::cout << " " << getElt(i) << std::endl;
	}
}