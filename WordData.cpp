/*
Assignment 1 - COMP 5421
Marc Belley - 5701295
*/

#include "WordData.h"
#include <iostream>
#include <string>

using namespace std;
// WordNode Constructor

WordData::WordData() {
	
}

WordData::WordData(std::string word,int lineNum) {
	wordChars = new char[word.length()+1];
    for (int i = 0;i < word.length()+1;i++) {
		if (i == (word.length())) {
			wordChars[i] = (char) 0;
		}
		else {
			wordChars[i] = word.at(i);
		}
    }
    appendLineNum(lineNum);
    wordFrequency = 1;
}

void WordData::appendLineNum(int lineNum) {
	// appends lineNum to IntList lineNums
	lineNums.appendInt(lineNum);
	wordFrequency++;
}

string WordData::lineNumString(){
    string str;
    //lineNums.printList(lineNums);
    for (int i = 0; i < lineNums.getSize(); i++){
		str.append(" ");
        str.append(std::to_string(lineNums.getElt(i)));
	}
    return str;
}

int WordData::getFrequency(){
	return wordFrequency;
}

char* WordData::getWord(){
	return wordChars;
}