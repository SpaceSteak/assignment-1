/*
Assignment 1 - COMP 5421
Marc Belley - 5701295
*/

//wordListDriver.cpp
#include <iostream>
using namespace std;
#include "WordList.h"
#include "IntList.h"

int main()
{
	WordList wl("input.txt");
    std::cout << "***************************" << std::endl;
	std::cout << "Welcome to WordList 5701295" << std::endl;
    std::cout << "***************************" << std::endl;
	wl.parseFile(cout);
	return 0;
}
